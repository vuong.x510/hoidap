import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { BackTop } from "antd";
import { UpOutlined } from "@ant-design/icons";

import AuthContextProvider from "./contexts/AuthContext";
import PostContextProvider from "./contexts/PostContext";
import AdminContextProvider from "./contexts/AdminContext";
import Header from "./components/Header/Header.js";
import Footer from "./components/Footer/Footer.js";
import ListQuestion from "./components/ListQuestion/ListQuestion.js";
import Home from "./components/Home/Home.js";
import MakeAQuestion from "./components/MakeAQuestion/MakeAQuestion";
import Error from "./components/Error/Error.js";
import "./App.css";
import AQuestion from "./components/AQuestion/AQuestion";
import UserManagement from "./components/Admin/UserManagement";
import QuestionManagement from "./components/Admin/QuestionManagement";
import ProtectedRoute from "./components/routing/ProtectedRoute";
import ScrollToTop from "./components/ScrollToTop";

class App extends Component {
  render() {
    return (
      <>
        <AuthContextProvider>
          <PostContextProvider>
            <BrowserRouter>
              <ScrollToTop />
              <Header />
              <Switch>
                <Route exact path="/" component={Home} />
                <Route
                  exact
                  path="/make_a_question"
                  component={MakeAQuestion}
                />

                <Route exact path="/list_question" component={ListQuestion} />
                <Route exact path="/a_question" component={AQuestion} />

                <AdminContextProvider>
                  <ProtectedRoute
                    exact
                    path="/admin/user"
                    component={UserManagement}
                  />
                  <ProtectedRoute
                    exact
                    path="/admin/question"
                    component={QuestionManagement}
                  />
                </AdminContextProvider>
                <Route path="*" component={Error} />
              </Switch>
              <Footer />
              <BackTop>
                <div
                  style={{
                    height: 40,
                    width: 40,
                    lineHeight: "40px",
                    borderRadius: 4,
                    backgroundColor: "#1088e9",
                    color: "#fff",
                    textAlign: "center",
                    fontSize: 14,
                  }}
                >
                  <UpOutlined />
                </div>
              </BackTop>
            </BrowserRouter>
          </PostContextProvider>
        </AuthContextProvider>
      </>
    );
  }
}

export default App;
