import { createContext, useReducer, useEffect } from 'react'
import axios from 'axios'

import setAuthToken from '../utils/setAuthToken'
import { authReducer } from '../reducers/authReducer'
import { apiUrl, LOCAL_STORAGE_TOKEN_NAME } from './constants'

export const AuthContext = createContext()

const AuthContextProvider = ({ children }) => {
	const [authState, dispatch] = useReducer(authReducer, {
		authLoading: true,
		isAuthenticated: false,
		user: null
	})

	// Authenticate user
	const loadUser = async () => {
		if (localStorage[LOCAL_STORAGE_TOKEN_NAME]) {
			setAuthToken(localStorage[LOCAL_STORAGE_TOKEN_NAME])
		}

		try {
			const response = await axios.get(`${apiUrl}/auth`)
			if (response.data.success) {
				dispatch({
					type: 'SET_AUTH',
					payload: { isAuthenticated: true, user: response.data.user }
				})
			}

		} catch (error) {
			localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
			setAuthToken(null)
			dispatch({
				type: 'SET_AUTH',
				payload: { isAuthenticated: false, user: null }
			})
		}
	}

	useEffect(() => loadUser(), [])

	// Login
	const loginUser = async userForm => {
		try {
			const response = await axios.post(`${apiUrl}/auth/login`, userForm)
			if (response.data.success)
				localStorage.setItem(
					LOCAL_STORAGE_TOKEN_NAME,
					response.data.accessToken
				)

			await loadUser()

			return response.data
		} catch (error) {
			if (error.response.data) return error.response.data
			else return { success: false, message: error.message }
		}
	}

	// Find username
	const findUser = async userForm => {
		try {
			const response = await axios.post(`${apiUrl}/auth/find`, userForm)
			return response.data
		} catch (error) {
			if (error.response.data) return error.response.data
			else return { success: false, message: error.message }
		}
	}

	// Register
	const registerUser = async userForm => {
		try {
			const response = await axios.post(`${apiUrl}/auth/register`, userForm)
			if (response.data.success)
				localStorage.setItem(
					LOCAL_STORAGE_TOKEN_NAME,
					response.data.accessToken
				)

			await loadUser()

			return response.data
		} catch (error) {
			if (error.response.data) return error.response.data
			else return { success: false, message: error.message }
		}
	}

	// Update
	const updateUserByAdmin = async dataUser => {
		try {
			const response = await axios.put(
				`${apiUrl}/auth/updateUserByAdmin`,
				dataUser
			)
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			return error.response.data
				? error.response.data
				: { success: false, message: 'Server error' }
		}
	}

	// Delete User
	// tai sao lai la the nay viet the nay {data: arrayUser} ???? that kho hieu
	const deleteUser = async arrayUser => {
		try {
			const response = await axios.delete(`${apiUrl}/auth/deleteUser`,{data: arrayUser} )
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			console.log(error)
		}
	}

	// Logout
	const logoutUser = () => {
		localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
		dispatch({
			type: 'SET_AUTH',
			payload: { isAuthenticated: false, user: null }
		})
		window.location.reload();
	}

	// Get all user testing
	const getAllUser = async () => {
		try {
			const response = await axios.get(`${apiUrl}/auth/all`)
			return response.data
		} catch (error) {
			console.log(error);
		}
	}

	// POST user by admin
	const registerByAdmin = async userData => {
		console.log(userData);
		try {
			const response = await axios.post(`${apiUrl}/auth/registerByAdmin`, userData)

			return response.data
		} catch (error) {
			console.log(error);
		}
	}


	// Context data
	const authContextData = { loginUser, registerUser, logoutUser, findUser, getAllUser, authState, updateUserByAdmin,loadUser,  registerByAdmin, deleteUser }

	// Return provider
	return (
		<AuthContext.Provider value={authContextData}>
			{children}
		</AuthContext.Provider>
	)
}

export default AuthContextProvider