import axios from "axios";
import { message } from "antd";
import { apiUrl } from "./constants";

// Add comment
export const addComment = async (idPost, newComment) => {
  console.log("comen", idPost, newComment);

  try {
    const response = await axios.post(
      `${apiUrl}/comment/${idPost}`,
      newComment
    );
    if (response.data.success) {
      message.success(response.data.message);
      return response.data;
    }
  } catch (error) {
    console.log(error);
    message.error("Hệ thống đang gập sự cố vui lòng thử lại sau!");
  }
};

// Get comment by id question
export const getComment = async (idPost) => {
  try {
    const response = await axios.get(`${apiUrl}/comment/${idPost}`);
    if (response.data.success) {
      return response.data.comment;
    }
  } catch (error) {
    console.log(error);
  }
};
