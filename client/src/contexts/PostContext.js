import { createContext, useReducer } from 'react'
import { postReducer } from '../reducers/postReducer'
import {
	apiUrl,
	POSTS_LOADED_FAIL,
	POSTS_LOADED_SUCCESS,
	ADD_POST,
	UPDATE_POST,
	FIND_POST
} from './constants'
import axios from 'axios'
import { message } from 'antd'

export const PostContext = createContext()

const PostContextProvider = ({ children }) => {
	// State
	const [postState, dispatch] = useReducer(postReducer, {
		post: null,
		posts: [],
		postsLoading: true
	})


	// Get all posts
	const getAllPosts = async () => {
		try {
			const response = await axios.get(`${apiUrl}/questions/all`)
			if (response.data.success) {
				return response.data.questions
			}
		} catch (error) {
			console.log(error);
		}
	}

	// Get my posts
	const getMyAllPosts = async () => {
		try {
			const response = await axios.get(`${apiUrl}/questions/myAll`)
			if (response.data.success) {
				return response.data.questions
			}
		} catch (error) {
			console.log(error);
		}
	}

	// Get all posts by doctor
	const allByDoctor = async () => {
		try {
			const response = await axios.get(`${apiUrl}/questions/allByDoctor`)
			if (response.data.success) {
				return response.data.questions
			}
		} catch (error) {
			console.log(error);
		}
	}

	// Get all posts by admin
	const getAdminAllPosts = async () => {
		try {
			const response = await axios.get(`${apiUrl}/questions/adminAll`)
			if (response.data.success) {
				return response.data.questions
			}
		} catch (error) {
			console.log(error);
		}
	}

	//Get one post
	const getOnePost = async postId => {
		try {
			const response = await axios.get(`${apiUrl}/questions/one/${postId}`)

			return response.data.questions

		} catch (error) {
			console.log(error);
		}
	}

	// Get all posts of user
	const getPosts = async () => {
		try {
			const response = await axios.get(`${apiUrl}/questions`)
			if (response.data.success) {
				dispatch({ type: POSTS_LOADED_SUCCESS, payload: response.data.posts })
			}
			return response.data.questions

		} catch (error) {
			dispatch({ type: POSTS_LOADED_FAIL })
		}
	}

	// Add post
	const addPost = async newPost => {
		try {
			const response = await axios.post(`${apiUrl}/questions`, newPost)
			if (response.data.success) {
				dispatch({ type: ADD_POST, payload: response.data.post })
				return response.data
			}
		} catch (error) {
			console.log(error)
			message.error(error.response.data.message);
		}
	}

	// Delete post
	const deleteQuestion = async array => {
		try {
			const response = await axios.delete(`${apiUrl}/questions/deleteQuestion`, { data: array })
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			console.log(error)
		}
	}

	// Find post when user is updating post
	const findPost = postId => {
		const post = postState.posts.find(post => post._id === postId)
		dispatch({ type: FIND_POST, payload: post })
	}

	// Update post
	const updatePost = async updatedPost => {
		try {
			const response = await axios.put(
				`${apiUrl}/posts/${updatedPost._id}`,
				updatedPost
			)
			if (response.data.success) {
				dispatch({ type: UPDATE_POST, payload: response.data.post })
				return response.data
			}
		} catch (error) {
			return error.response.data
				? error.response.data
				: { success: false, message: 'Server error' }
		}
	}

	// Update post by doctor
	const doctorUpdate = async (updatedPost, id) => {
		try {
			const response = await axios.put(
				`${apiUrl}/questions/doctorUpdate/${id}`,
				updatedPost
			)
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			return error.response.data
				? error.response.data
				: { success: false, message: 'Server error' }
		}
	}

	// Add post admin
	const postQuestion = async newPost => {
		try {
			const response = await axios.post(`${apiUrl}/questions/postQuestion`, newPost)
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			console.log(error)
		}
	}

	// Update  admin
	const updateQuestion = async updatedPost => {
		try {
			const response = await axios.put(
				`${apiUrl}/questions/updateQuestion`,
				updatedPost
			)
			if (response.data.success) {
				return response.data
			}
		} catch (error) {
			console.log(error);
		}
	}

	// Post context data
	const postContextData = {
		postState,
		getPosts,
		allByDoctor,
		addPost,
		deleteQuestion,
		findPost,
		updatePost,
		getAllPosts,
		getOnePost,
		postQuestion,
		updateQuestion,
		getMyAllPosts,
		getAdminAllPosts,
		doctorUpdate
	}

	return (
		<PostContext.Provider value={postContextData}>
			{children}
		</PostContext.Provider>
	)
}

export default PostContextProvider
