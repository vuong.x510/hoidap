import { createContext, useState, } from 'react'
import axios from 'axios'

import { apiUrl } from './constants'

export const AdminContext = createContext()

const AdminContextProvider = ({ children }) => {
  const [modalState, setModalState] = useState({
    showAddUser: false,
    showUpdateUser: false,
    showAddQuestion: false,
    showUpdateQuestion: false,
    modalAddUser: false,
    modalUpdateUser: false,
    modalAddQuestion: false,
    modalUpdateQuestion: false,
  })

  const allUserAsk = async () => {
    try {
      const response = await axios.get(`${apiUrl}/auth/allUserAsk`)
      return response.data
    } catch (error) {
      console.log(error);
    }
  }

  // Return provider
  return (
    <AdminContext.Provider value={{ modalState, setModalState, allUserAsk }}>
      {children}
    </AdminContext.Provider>
  )

}

export default AdminContextProvider