import React, { useEffect, useState, useContext } from "react";
import { useLocation } from "react-router-dom";
import { Tooltip, Image, List, Avatar, Space, Button, message } from "antd";
import { LikeOutlined, CheckCircleOutlined } from "@ant-design/icons";
import "moment/locale/vi";
import moment from "moment";
import FileBase from "react-file-base64";

import ImgUser from "../../assets/avatar.png";
import { PostContext } from "../../contexts/PostContext";
import { AuthContext } from "../../contexts/AuthContext";
import { addComment, getComment } from "../../contexts/CommentContext";

import "./style.css";

const IconText = ({ icon, text }) => (
  <Space>
    {React.createElement(icon)}
    {text}
  </Space>
);

export default function AQuestion() {
  let addEComment = null;
  let query = useQuery().get("post");
  let query2 = useQuery().get("u");

  const [commentData, setCommentData] = useState({ content: "", image: "" });
  const [dataQuestion, setDataQuestion] = useState({
    dataLoading: true,
    data: [],
  });

  const { authState } = useContext(AuthContext);
  const { getOnePost } = useContext(PostContext);

  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  console.log(authState);

  async function fetchData() {
    const data = await getOnePost(query);
    const dataC = await getComment(query);
    setDataQuestion({ dataLoading: false, data: [data, ...dataC] });
  }

  useEffect(() => {
    fetchData();
  }, [getOnePost, query]);

  useEffect(() => {
    let time = setTimeout(() => {
      fetchData();
    }, 4000);
    return () => {
      clearTimeout(time);
    };
  });

  const onSubmit = async () => {
    if (!commentData.content)
      return message.warning("Nội dung trả lời không được trống!");

    try {
      const data = await addComment(
        `${query}&${authState.user._id}`,
        commentData
      );
      if (data.success) {
        setDataQuestion({
          dataLoading: false,
          data: [
            ...dataQuestion.data,
            { ...authState, ...commentData, createdAt: Date.now() },
          ],
        });
        setCommentData({ content: "", image: "" });
      }
    } catch (error) {
      console.log(error);
    }
  };

  console.log(dataQuestion.data);

  if (authState.user) {
    if (
      !dataQuestion.loading &&
      dataQuestion.data[0]?.status === "approved" &&
      (authState.user._id === query2 || authState.user.role === 1)
    ) {
      addEComment = (
        <>
          <hr className="mg-hr" />
          <div className="comment-inner">
            <div className="comment-inner-avatar">
              <img
                src={!authState.user.image ? ImgUser : authState.user.image}
                alt="anh user"
              />
            </div>
            <div className="comment-detail">
              <div className="mg-bt">
                <FileBase
                  type="file"
                  multiple={false}
                  value={commentData.image}
                  onDone={({ base64 }) =>
                    setCommentData({ ...commentData, image: base64 })
                  }
                />
              </div>
              <div>
                <textarea
                  name=""
                  id=""
                  className="input-comment"
                  rows="4"
                  value={commentData.content}
                  onChange={(e) =>
                    setCommentData({ ...commentData, content: e.target.value })
                  }
                ></textarea>
              </div>
              <div className="comment-sbm">
                <Button onClick={onSubmit} type="primary">
                  Trả lời
                </Button>
              </div>
            </div>
          </div>
        </>
      );
    }
  }

  return (
    <div>
      <div className="lq_container min-h">
        <List
          itemLayout="vertical"
          size="large"
          className="qs-bold"
          dataSource={dataQuestion.data}
          loading={dataQuestion.dataLoading}
          renderItem={(item) => (
            <List.Item
              key={item.content}
              actions={[
                <IconText
                  icon={LikeOutlined}
                  text="0"
                  key="list-vertical-like-o"
                />,
              ]}
              extra={
                <Image.PreviewGroup>
                  <Image width={300} src={item.image} />
                </Image.PreviewGroup>
              }
            >
              <List.Item.Meta
                avatar={
                  <Avatar
                    style={{ width: "70px", height: "70px" }}
                    src={!item.user.image ? ImgUser : item.user.image}
                  />
                }
                title={
                  <>
                    <span className="author-qs nameauth">
                      {item.user.fullName
                        ? item.user.fullName
                        : item.user.username}
                      {item.user.role === 1 ? (
                        <CheckCircleOutlined
                          style={{ color: "green", marginLeft: "5px" }}
                        />
                      ) : (
                        ""
                      )}
                    </span>
                    <Tooltip
                      title={moment(item.createdAt).format(
                        "YYYY-MM-DD HH:mm:ss"
                      )}
                    >
                      <span className="author-qs" style={{ color: "#ccc" }}>
                        {moment(item.createdAt).fromNow()}
                      </span>
                    </Tooltip>
                  </>
                }
                description={
                  <p className="conten-qs qs-bold">{item.content}</p>
                }
              />
            </List.Item>
          )}
        />
        {addEComment}
      </div>
    </div>
  );
}
