import React, { useContext, useEffect, useState } from "react";
import moment from "moment";
import {
  Tooltip,
  Image,
  List,
  Avatar,
  Space,
  Radio,
  Button,
  Input,
  message,
  Modal,
} from "antd";
import {
  MessageOutlined,
  LikeOutlined,
  UngroupOutlined,
  SearchOutlined,
  CheckSquareOutlined,
  CloseSquareOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import "moment/locale/vi";
import { Link } from "react-router-dom";

import { PostContext } from "../../contexts/PostContext";
import { AuthContext } from "../../contexts/AuthContext";

import ImgUser from "../../assets/avatar.png";
import "./style.css";

const ListQuestion = () => {
  const [dataQuestions, setDataQuestions] = useState({
    dataLoading: true,
    data: [],
  });
  const [filter, setFilter] = useState(1);
  const [textSearch, setTextSearch] = useState("");
  const [myQuestion, setMyQuestion] = useState([]);
  const [allQuestion, setAllQuestion] = useState([]);

  const { confirm } = Modal;

  // Context
  const { getAllPosts, getMyAllPosts, allByDoctor, doctorUpdate } =
    useContext(PostContext);
  const { authState } = useContext(AuthContext);

  useEffect(() => {
    async function fetchData() {
      if (filter == 1) {
        setDataQuestions({ ...dataQuestions, dataLoading: true });
        const data =
          authState.user && authState.user.role == 1
            ? await allByDoctor()
            : await getAllPosts();
        await setAllQuestion(data);
        await setDataQuestions({ dataLoading: false, data });
      } else {
        setDataQuestions({ ...dataQuestions, dataLoading: true });
        const data = await getMyAllPosts();
        await setMyQuestion(data);
        await setDataQuestions({ dataLoading: false, data });
      }
    }
    fetchData();
    window.scrollTo(0, 0);
  }, [filter, authState]);

  const IconText = ({ icon, text }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );
  console.log(dataQuestions);

  const filterQuestion = (textSearch) => {
    if (dataQuestions.data && textSearch) {
      if (filter === 1) {
        const data = allQuestion.filter((item) => {
          return item.content
            .toLowerCase()
            .includes(textSearch.trim().toLowerCase().replace(/\s\s+/g, " "));
        });
        setDataQuestions({ dataLoading: false, data });
      } else {
        const data = myQuestion.filter((item) => {
          return item.content
            .toLowerCase()
            .includes(textSearch.trim().toLowerCase().replace(/\s\s+/g, " "));
        });
        setDataQuestions({ dataLoading: false, data });
      }
    }
  };

  const searchOnChange = (text) => {
    if (text === "") {
      filter === 1
        ? setDataQuestions({ dataLoading: false, data: allQuestion })
        : setDataQuestions({ dataLoading: false, data: myQuestion });
      return;
    }
    setTextSearch(text);
  };

  const changeStatus = (status, id, action) => {
    confirm({
      title: action,
      okText: "Đồng ý",
      cancelText: "Hủy",
      icon: <ExclamationCircleOutlined />,
      content: (
        <>
          Bạn chắc chắn muốn
          <span style={{ textTransform: "lowercase" }}>{` ${action}`}</span>?
        </>
      ),
      async onOk() {
        try {
          const data = await doctorUpdate({ status }, id);
          if (data.success) {
            const data = await allByDoctor();
            await setAllQuestion(data);
            await setDataQuestions({ dataLoading: false, data });
            message.success(`${action} thành công!`);
          } else message.success(data.message);
        } catch (error) {
          console.log(error);
        }
      },
      onCancel() {},
    });
  };

  const actions = (item) => {
    if (filter === 2 && authState.user) {
      return [
        <IconText icon={LikeOutlined} text="0" key="list-vertical-like-o" />,
        <IconText
          icon={MessageOutlined}
          text="0"
          key="list-vertical-message"
        />,
        <Link to={`/a_question?post=${item._id}&u=${item.user._id}`}>
          <IconText
            icon={UngroupOutlined}
            text="Xem chi tiết"
            key="list-vertical-message1"
          />
        </Link>,
        <span
          className={
            item.status === "approval"
              ? "vang"
              : item.status === "hide"
              ? ""
              : "vang2"
          }
        >
          <IconText
            icon={CheckSquareOutlined}
            text={
              item.status === "approval" ? (
                <span className="vang">Đang chờ duyệt câu hỏi</span>
              ) : item.status === "hide" ? (
                <span className="vang3">Bị đánh dấu spam</span>
              ) : (
                <span className="vang2">Đã được duyệt</span>
              )
            }
            key="list-vertical-message22"
          />
        </span>,
      ];
    }
    if (authState.user && authState.user.role === 1) {
      if (item.status === "approval")
        return [
          <IconText icon={LikeOutlined} text="0" key="list-vertical-like-o" />,
          <IconText
            icon={MessageOutlined}
            text="1"
            key="list-vertical-message"
          />,
          <Link to={`/a_question?post=${item._id}&u=${item.user._id}`}>
            <IconText
              icon={UngroupOutlined}
              text="Xem chi tiết"
              key="list-vertical-message1"
            />
          </Link>,
          <span
            onClick={() => changeStatus("approved", item._id, "Duyệt câu hỏi")}
            className="dotter-check green"
          >
            <IconText
              icon={CheckSquareOutlined}
              text="Duyệt câu hỏi"
              key="list-vertical-message2"
            />
          </span>,
          <span
            onClick={() => changeStatus("hide", item._id, "Loại bỏ câu hỏi")}
            className="dotter-check red"
          >
            <IconText
              icon={CloseSquareOutlined}
              text="Loại bỏ câu hỏi"
              key="list-vertical-message3"
            />
          </span>,
        ];

      if (item.status === "approved") {
        return [
          <IconText icon={LikeOutlined} text="0" key="list-vertical-like-o" />,
          <IconText
            icon={MessageOutlined}
            text="0"
            key="list-vertical-message"
          />,
          <Link to={`/a_question?post=${item._id}&u=${item.user._id}`}>
            <IconText
              icon={UngroupOutlined}
              text="Xem chi tiết"
              key="list-vertical-message1"
            />
          </Link>,

          <span className=" green">
            <IconText
              icon={CheckSquareOutlined}
              text="Đã kiểm duyệt"
              key="list-vertical-message2"
            />
          </span>,
          <span
            onClick={() => changeStatus("hide", item._id, "Loại bỏ câu hỏi")}
            className="dotter-check red"
          >
            <IconText
              icon={CloseSquareOutlined}
              text="Loại bỏ câu hỏi"
              key="list-vertical-message3"
            />
          </span>,
        ];
      }
    }

    return [
      <IconText icon={LikeOutlined} text="0" key="list-vertical-like-o" />,
      <IconText icon={MessageOutlined} text="1" key="list-vertical-message" />,
      <Link to={`/a_question?post=${item._id}&u=${item.user._id}`}>
        <IconText
          icon={UngroupOutlined}
          text="Xem chi tiết"
          key="list-vertical-message1"
        />
      </Link>,
    ];
  };

  return (
    <>
      <div className="lq_container min-h">
        <div className="filter">
          <div className="filter">
            <div>
              <span className="filter__title">Lọc câu hỏi</span>
            </div>
            <Radio.Group
              onChange={(e) => setFilter(e.target.value)}
              value={filter}
            >
              {authState.user?.role < 1 && (
                <Radio value={2}>Câu hỏi bạn đã đặt</Radio>
              )}
              <Radio value={1}>Toàn bộ câu hỏi</Radio>
            </Radio.Group>
          </div>
          <div className="search">
            <Input
              allowClear="true"
              placeholder="Nhập câu hỏi cần tìm kiếm"
              maxLength={25}
              onChange={(e) => searchOnChange(e.target.value)}
            />
            <Button
              type="primary"
              icon={<SearchOutlined />}
              onClick={() => filterQuestion(textSearch)}
            >
              Tìm kiếm
            </Button>
          </div>
        </div>
        <div className="list-ps">
          <List
            itemLayout="vertical"
            size="large"
            pagination={{
              onChange: (page) => {
                console.log(page);
                window.scrollTo(0, 0);
              },
              pageSize: 7,
            }}
            loading={dataQuestions.dataLoading}
            dataSource={dataQuestions.data}
            renderItem={(item) => {
              return (
                <List.Item
                  key={item.title}
                  actions={actions(item)}
                  extra={
                    <Image.PreviewGroup>
                      <Image width={300} src={item.image} />
                    </Image.PreviewGroup>
                  }
                >
                  <List.Item.Meta
                    style={{ margin: "0px" }}
                    avatar={
                      <Avatar
                        style={{ width: "70px", height: "70px" }}
                        src={!item.user.image ? ImgUser : item.user.image}
                      />
                    }
                    title={
                      <>
                        <span className="author-qs nameauth">
                          {item.user.fullName
                            ? item.user.fullName
                            : item.user.username}
                        </span>
                        <Tooltip
                          title={moment(item.createdAt)
                            .startOf("days")
                            .format("YYYY-MM-DD HH:mm:ss")}
                        >
                          <span className="author-qs" style={{ color: "#ccc" }}>
                            {moment(item.createdAt).startOf("days").fromNow()}
                          </span>
                        </Tooltip>
                      </>
                    }
                    description={<p className="conten-qs">{item.content}</p>}
                  />
                </List.Item>
              );
            }}
          />
        </div>
      </div>
    </>
  );
};

export default ListQuestion;
