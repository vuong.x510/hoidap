import React, { useContext, useState, useEffect } from "react";
import { Button, Badge, Menu, Dropdown, Popover, Avatar } from "antd";
import {
  NotificationOutlined,
  UserOutlined,
  LogoutOutlined,
  CaretDownOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

import AuthRegister from "../Auth/AuthRegister";
import AuthLogin from "../Auth/AuthLogin";
import { AuthContext } from "../../contexts/AuthContext";
import "./style.css";
import ImgUser from "../../assets/avatar.png";
import Logo from "../../assets/logo.png";

const Header = () => {
  // userState
  const [overlay, setOverlay] = useState("none");
  const [modal, setModal] = useState(null);
  const [drop, setDrop] = useState("none");

  const { authState, logoutUser } = useContext(AuthContext);

  const logout = () => logoutUser();

  const show = (modal) => {
    setOverlay("block");
    modal === "login" ? setModal(<AuthLogin />) : setModal(<AuthRegister />);
  };

  const showed = () => {
    setOverlay("none");
    setModal("");
  };

  let admin = null;

  let form = (
    <div className="header_auth">
      <div
        className="auth_overlay"
        style={{ display: `${overlay}` }}
        onClick={() => showed()}
      ></div>
      {modal}
    </div>
  );

  let body = null;

  const menu = (
    <Menu style={{ top: "-4px" }}>
      <Menu.Item key="0">
        <Link to="/admin/question" style={{ color: "#1890ff" }}>
          QUẢN LÝ CÂU HỎI
        </Link>
      </Menu.Item>
      <Menu.Item key="1">
        <Link to="/admin/user" style={{ color: "#1890ff" }}>
          QUẢN LÝ USER
        </Link>
      </Menu.Item>
    </Menu>
  );

  const content = (
    <div style={{ padding: 0 }}>
      {/* <span className="item">
        <UserOutlined style={{ marginRight: "5px" }} /> Thông tin cá nhân
      </span> */}
      <span className="item" onClick={() => logout()}>
        <LogoutOutlined style={{ marginRight: "5px" }} /> Đăng xuất
      </span>
    </div>
  );

  if (authState.user != null) {
    body = (
      <>
        <Badge count={0} size="small" overflowCount={100} offset={[0, 5]}>
          <NotificationOutlined
            style={{
              fontSize: "20px",
              lineHeight: "20px",
              color: "#F38B35",
            }}
          />
        </Badge>
        <Popover content={content} trigger="click" style={{ padding: 0 }}>
          <span
            style={{
              paddingLeft: "15px",
              marginLeft: "15px",
              borderLeft: "1px solid #F38B35",
            }}
          >
            <span>
              <Avatar
                src={authState.user.image ? authState.user.image : ImgUser}
              />
            </span>
            {/* <CaretDownOutlined /> */}
            <span className="username">
              {" "}
              {authState.user.fullName
                ? authState.user.fullName
                : authState.user.username}
            </span>
          </span>
        </Popover>
      </>
    );

    form = null;
    if (authState.user.role == 2) {
      admin = (
        <>
          <li className="header_item">
            <Dropdown overlay={menu} placement="bottomLeft">
              <span className="ql">QUẢN LÝ</span>
            </Dropdown>
          </li>
        </>
      );
    }
  } else {
    body = (
      <>
        <Button
          style={{ marginRight: "5px", borderRadius: "15px" }}
          onClick={() => show("register")}
        >
          Đăng ký
        </Button>
        <Button
          type="primary"
          style={{ borderRadius: "15px" }}
          onClick={() => show("login")}
        >
          Đăng nhập
        </Button>
      </>
    );
  }

  useEffect(() => {
    if (authState.user != null) {
      setOverlay("none");
      setModal("");
    } else {
      setDrop("none");
    }
  });

  return (
    <header>
      <div className="header_container">
        <div className="header">
          <div className="header_right">
            <span className="logo">
              <img src={Logo} alt="logo" height="52px" width="auto" />
            </span>
          </div>
          <nav className="header_mid">
            <ul className="header_list">
              <li className="header_item">
                <Link to="/">Trang chủ</Link>
              </li>
              <li className="header_item">
                <Link to="/make_a_question">Đặt câu hỏi</Link>
              </li>
              <li className="header_item">
                <Link to="/list_question">Danh sách câu hỏi</Link>
              </li>
              {admin}
            </ul>
          </nav>
          <div className="header_right">{body}</div>
        </div>
      </div>
      {form}
    </header>
  );
};

export default Header;
