import React from "react";
import "./style.css";

const Error = () => {
  return (
    <div class="lq_container err_style">
      <div class="NotFound_content__1TxgM">
        <h1>Awww... Trang này không tồn tại :(</h1>
        <p>Chúng tôi không thể tìm thấy trang mà bạn đang tìm kiếm.</p>
        <ul>
          <li>Hãy thử làm mới trang, sự cố này có thể chỉ là tạm thời.</li>
          <li>
            Nếu bạn đã nhập địa chỉ theo cách thủ công, hãy kiểm tra lại xem nó
            có chính xác không.
          </li>
        </ul>
        <p>
          Quay về <a href="/">Trang Chủ Hỏi Đáp Bác Sĩ</a>
        </p>
        <p class="NotFound_copyright__3-UNA">
          © 2021 vuonglk. All rights reserved.
        </p>
      </div>
      <div class="NotFound_overlay__3AyA2"></div>
    </div>
  );
};

export default Error;
