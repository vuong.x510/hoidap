import React, { Component } from "react";
import { SendOutlined, CheckOutlined } from "@ant-design/icons";
import { Button, Carousel, Avatar } from "antd";
import { Link } from "react-router-dom";

import ImgDoctor from "../../assets/img-doctor-maq.png";
import Bacsi1 from "../../assets/bacsi1.jpeg";
import Bacsi2 from "../../assets/bacsi2.jpeg";
import Bacsi3 from "../../assets/bacsi3.jpg";
import Chatdortor from "../../assets/chatdoctor.jpg";

import "./style.css";
class Home extends Component {
  render() {
    return (
      <div>
        <section className="ask">
          <div className="ask_background-overlay"></div>
          <div className="ask_container">
            <div className="ask_row">
              <div className="ask_col-left">
                <p className="ask_subtitle">
                  Chia sẻ vấn đề sức khoẻ của bạn với bác sĩ{" "}
                </p>
                <h2 className="ask_title">
                  Hỏi Bác sĩ Chuyên khoa hô hấp và phổi bất cứ điều gì
                </h2>
                <p className="ask_text">
                  Hãy nói về sức khỏe của bạn với các triệu chứng, loại thuốc
                  hiện tại, giới tính, tuổi, chiều cao và cân nặng
                </p>
                <div>
                  <Link to="/make_a_question">
                    <Button
                      type="primary"
                      size="large"
                      icon={<SendOutlined />}
                      style={{ background: "#F88B35", borderColor: "#F88B35" }}
                    >
                      {" "}
                      Đặt câu hỏi ngay
                    </Button>
                  </Link>
                </div>
              </div>
              <div className="ask_col-right">
                <img
                  src={ImgDoctor}
                  alt="img-doctor"
                  width="362"
                  height="300"
                />
              </div>
            </div>
          </div>
        </section>

        <div className="contai">
          <Carousel autoplay>
            <div>
              <div className="info-doctor-container">
                <Avatar
                  src={Bacsi1}
                  style={{ width: "170px", height: "170px" }}
                />
                <section className="info-doctor">
                  <h4 className="info-doctor-title">
                    " Các bác sĩ luôn online 24/24h để tiếp nhận thông tin và
                    kịp thời đưa ra nhưng lời khuyên tốt nhất! "
                  </h4>
                  <p className="info-doctor-job">
                    <span className=" ">Th.S BS Dr Daryl Tan</span>
                    <br /> <i> Trưởng Khoa Phổi</i>
                  </p>
                </section>
              </div>
            </div>

            <div>
              <div className="info-doctor-container">
                <Avatar
                  src={Bacsi2}
                  style={{ width: "170px", height: "170px" }}
                />
                <section className="info-doctor">
                  <h4 className="info-doctor-title">
                    " Các bác sĩ giàu kinh nghiệm của chúng tôi đã cung cấp lời
                    khuyên y tế chất lượng cho những người có nhu cầu từ năm
                    2020. "
                  </h4>
                  <p className="info-doctor-job">
                    <span className="info-doctor-name">Th.S BS Lê Bảo Đại</span>
                    <br />
                    <i> Trưởng Phó Khoa Hô Hấp </i>
                  </p>
                </section>
              </div>
            </div>

            <div>
              <div className="info-doctor-container">
                <Avatar
                  src={Bacsi3}
                  style={{ width: "170px", height: "170px" }}
                />
                <section className="info-doctor">
                  <h4 className="info-doctor-title">
                    " Truy cập tức thì các bác sĩ trong mạng từ bất kỳ máy tính
                    hoặc điện thoại thông minh nào kết nối với bác sĩ trong vài
                    giây để có một cuộc trò chuyện chăm sóc với bác sĩ. "
                  </h4>
                  <p className="info-doctor-job">
                    <span className="info-doctor-name">
                      Giáo sư Nguyễn Hà Linh
                    </span>
                    <br /> <i> Giám Đốc bệnh viện ABS </i>
                  </p>
                </section>
              </div>
            </div>
          </Carousel>
        </div>

        <div className="introduce">
          <div className="contai-home">
            <div className="home-img">
              <img src={Chatdortor} alt="anh" width="350px" height="auto" />
            </div>
            <section className="introduce-web">
              <h4 className="introduce-web-name">
                Truy cập nhanh chóng, kết nối với bác sĩ tức thì
              </h4>
              <div className="fat"></div>
              <p className="introduce-web-item">
                <CheckOutlined /> Người bệnh được hỗ trợ chăm sóc y tế an toàn,
                thoải mái ngay tại nhà.
              </p>
              <p className="introduce-web-item">
                <CheckOutlined /> Chăm sóc bệnh nhân tốt hơn.
              </p>
              <p className="introduce-web-item">
                <CheckOutlined /> Tăng cường uy tín trực tiếp.
              </p>
              <p className="introduce-web-item">
                <CheckOutlined /> Người bệnh được kiểm tra, chăm sóc định kỳ và
                có kế hoạch điều trị bởi chuyên gia điều trị bệnh mãn tính và
                đội ngũ nhân viên chăm sóc sức khỏe riêng..
              </p>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
