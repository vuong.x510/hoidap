import { useContext } from 'react';
import { Form, Input, Button, message } from 'antd';
import { GooglePlusOutlined } from '@ant-design/icons';

import { AuthContext } from '../../contexts/AuthContext';
import "./style.css";

const AuthRegister = () => {
  // Context
  const { registerUser, findUser } = useContext(AuthContext);

  const onFinish = async (values) => {
    try {
      const registerData = await registerUser({ username: values.username, password: values.password });
      if (registerData.success) {
        message.success('Chaò mừng đến với Hello bác sĩ!');
      }

    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
    }
  };

  return (
    <div className="auth">
      <h3 className="auth_title">Đăng ký</h3>
      <Form
        name="basic"
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên tài khoản!',
            },
            () => ({
              async validator(_, value) {
                if (value !== undefined) {
                  if (/\s/.test(value)) {
                    return Promise.reject(new Error('Tên tài khoản phải không có ký tự khoảng trắng!'));
                  }

                  if (value.length < 6 || value.length >= 15) {
                    return Promise.reject(new Error('Tên tài khoản phải có 6-15 ký tự!'));
                  }

                  try {
                    const data = await findUser({ username: value })
                    if (!data.success) {
                      return Promise.reject(new Error('Tên tài khoản đã tồn tại!'))
                    }
                    else
                      return Promise.resolve();

                  } catch (error) {
                    console.log(error);
                  }
                }
              },
            })
          ]}
        >
          <Input placeholder="Tên tài khoản" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập mật khẩu!',
            },
            () => ({
              validator(_, value) {
                if (!value || value.length >= 8 && value.length <= 16) {

                  return Promise.resolve();
                }
                return Promise.reject(new Error('Mật khẩu phải đảm bảo độ dài 8-16 ký tự!'));
              },
            }),
          ]}
        >
          <Input.Password placeholder="Mật khẩu" />
        </Form.Item>
        <Form.Item
          name="confirmPassword"
          dependencies={['password']}
          rules={[
            {
              required: true,
              message: 'Vui lòng lại nhập mật khẩu!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(new Error('Hai mật khẩu bạn đã nhập không khớp'));
              },
            }),
          ]}
        >
          <Input.Password placeholder="Nhập lại mật khẩu" />
        </Form.Item>
        <Form.Item >
          <Button type="primary" className="auth_sub" htmlType="submit">
            ĐĂNG KÝ
          </Button>
        </Form.Item>
      </Form>
      <div className="auth_line">
        <div className="line"></div>
        <span className="t">HOẶC</span>
        <div className="line"></div>
      </div>
      <div>
        <Button type="" className="auth_google" icon={<GooglePlusOutlined />}> Đăng nhập bằng Google</Button>
      </div>
      <div className="auth_move">
        <span className="auth_move-text">Bạn đã có tài khoản?</span>
        <span className="auth_move-click">Đăng nhập</span>
      </div>
    </div>
  );

}

export default AuthRegister;