import { useContext } from 'react';
import { Form, Input, Button, message } from 'antd';
import { GooglePlusOutlined } from '@ant-design/icons';

import { AuthContext } from '../../contexts/AuthContext';
import "./style.css";

const AuthLogin = () => {
  // Context
  const { loginUser } = useContext(AuthContext);

  const onFinish = async (values) => {
    try {
      const data = await loginUser({ username: values.username, password: values.password });
      if (data.success) {
        message.success('Đăng nhập thành công!');
      }
      else
        message.error("Thông tin tài khoản sai hoặc mật khẩu không chính xác!")
    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
    }
  };

  return (
    <div className="auth">
      <h3 className="auth_title">Đăng nhập</h3>
      <Form
        name="basic"
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên tài khoản!',
            },
          ]}
        >
          <Input placeholder="Tên tài khoản" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập mật khẩu!',
            },
          ]}
        >
          <Input.Password placeholder="Mật khẩu" />
        </Form.Item>
        <Form.Item className="btn-login" >
          <Button type="primary" className="auth_sub" htmlType="submit">
            ĐĂNG NHẬP
          </Button>
        </Form.Item>
      </Form>
      <div className="forgot">
        Quên mật khẩu
      </div>
      <div className="auth_line">
        <div className="line"></div>
        <span className="t">HOẶC</span>
        <div className="line"></div>
      </div>
      <div>
        <Button type="" className="auth_google" icon={<GooglePlusOutlined />}> Đăng nhập bằng Google</Button>
      </div>
      <div className="auth_move">
        <span className="auth_move-text">Bạn mới biết đến Hellobacsi?</span>
        <span className="auth_move-click">Đăng ký</span>
      </div>
    </div>
  );

}

export default AuthLogin;