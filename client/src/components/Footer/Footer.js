import React, { Component } from 'react';
import "./style.css"

import { Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <>
        <footer className="site-footer">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <h6>VỀ CHÚNG TÔI</h6>
                <p className="text-justify">Website<i> HỎI ĐÁP BÁC SĨ </i> là nền tảng Website hỏi đáp hoàn toàn miễn phí. Là nơi người bệnh đặt câu hỏi cho bác sĩ để giả đáp thắc mắc về bệnh tật của bản thân, gia đình .</p>
              </div>

              <div className="col-xs-6 col-md-3">
                <h6>Danh mục</h6>
                <ul className="footer-links">
                  <li><Link to="/">Trang chủ</Link></li>
                  <li><Link to="/make_a_question">Đặt câu hỏi</Link></li>
                  <li><Link to="/list_question">Danh sách câu hỏi</Link></li>
                </ul>
              </div>
            </div>
            <hr />
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-sm-6 col-xs-12">
                <p className="copyright-text">Copyright © 2017 All Rights Reserved by
                  <Link to="#"> vuonglk</Link>.
                </p>
              </div>
              <div className="col-md-4 col-sm-6 col-xs-12">
                <ul className="social-icons">
                  <li><Link className="facebook" to="#"><i className="fa fa-facebook" /></Link></li>
                  <li><Link className="twitter" to="#"><i className="fa fa-twitter" /></Link></li>
                  <li><Link className="dribbble" to="#"><i className="fa fa-dribbble" /></Link></li>
                  <li><Link className="linkedin" to="#"><i className="fa fa-linkedin" /></Link></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </>
    );
  }
}

export default Footer;