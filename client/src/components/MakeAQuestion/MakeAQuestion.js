import React, { useState, useContext, useRef, useEffect } from "react";
import {
  SendOutlined,
  FileImageOutlined,
  CheckCircleTwoTone,
} from "@ant-design/icons";
import { Button, message } from "antd";
import FileBase from "react-file-base64";
import { useHistory } from "react-router-dom";

import ImgDoctor from "../../assets/tasking-doctor.png";
import ImgRegister from "../../assets/register.jpg";
import "./style.css";
import { PostContext } from "../../contexts/PostContext";

const MakeAQuestion = () => {
  const [postData, setPostData] = useState({ content: "", image: "" });
  const [loading, setLoading] = useState(false);
  let history = useHistory();
  let inputEl = useRef();

  // Context
  const { addPost } = useContext(PostContext);

  const onSubmit = async () => {
    if (postData.content === "")
      return message.warning("Nội dung câu hỏi không được trống!");

    if (postData.content.length < 20)
      return message.warning(
        "Câu hỏi của bạn quá ngắn, vui lòng chi tiết hơn!"
      );

    try {
      setLoading(true);
      const data = await addPost(postData);
      if (data && data.success) {
        message.success(data.message);
        history.push(`/a_question?post=${data.data._id}&u=${data.data.user}`);
      }
      setLoading(false);
    } catch (error) {
      message.error("Hệ thống đang gập sự cố vui lòng thử lại sau!");
      setLoading(false);
    }
  };

  useEffect(() => {
    inputEl.current.focus();
  }, []);

  return (
    <div>
      <section>
        <div className="maq_container">
          <div className="maq_row">
            <div className="maq_col-left">
              <h2 className="maq_title">Hỏi đáp bác sĩ trực tuyến</h2>
              <p className="maq_subtitle">
                Chia sẻ vấn đề sức khoẻ của bạn với bác sĩ *
              </p>
              <form autoComplete="off" noValidate>
                <textarea
                  ref={inputEl}
                  name=""
                  id="content_questions"
                  cols=""
                  rows="7"
                  placeholder="Hãy nói về sức khỏe của bạn với các triệu chứng, loại thuốc hiện tại, giới tính, tuổi, chiều cao và cân nặng."
                  value={postData.content}
                  onChange={(e) =>
                    setPostData({ ...postData, content: e.target.value })
                  }
                ></textarea>

                <p className="maq_subtitle">
                  {" "}
                  <FileImageOutlined /> Đính kèm ảnh về tình trạng sức khỏe của
                  bạn
                </p>

                <FileBase
                  type="file"
                  multiple={false}
                  accept="image/png, image/jpeg"
                  value={postData.image}
                  onDone={({ base64 }) =>
                    setPostData({ ...postData, image: base64 })
                  }
                />

                <div>
                  <Button
                    type="primary"
                    size="large"
                    icon={<SendOutlined />}
                    loading={loading}
                    style={{
                      background: "#F88B35",
                      borderColor: "#F88B35",
                      display: "block",
                      width: "100%",
                    }}
                    onClick={() => onSubmit()}
                  >
                    Đặt câu hỏi
                  </Button>
                </div>
              </form>
            </div>
            <div className="maq_col-right">
              <div>
                <img
                  src={ImgDoctor}
                  alt="anh-doctor"
                  style={{ width: "400px", height: "400px" }}
                />
              </div>
              <div>
                <p className="maq_online">
                  <CheckCircleTwoTone twoToneColor="#52c41a" /> Có 0 bác sĩ trực
                  tuyến
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="maq_container-introduce">
          <div className="map_introduce-row">
            <div>
              <img
                src={ImgRegister}
                alt="anh bac si"
                style={{ width: "270px", height: "320px" }}
              />
            </div>
            <div className="maq_introduce-right">
              <h2 className="maq_introduce-title">
                Đặt câu hỏi cho bác sĩ giống như bạn nói chuyện với cho một
                người bạn.
              </h2>
              <p className="maq_introduce-text">
                Truy cập tức thì các bác sĩ trong mạng từ bất kỳ máy tính hoặc
                điện thoại thông minh nào kết nối với bác sĩ trong vài giây để
                có một cuộc trò chuyện chăm sóc với bác sĩ{" "}
              </p>
              <p className="maq_introduce-text">
                Các bác sĩ giàu kinh nghiệm của chúng tôi đã cung cấp lời khuyên
                y tế chất lượng cho những người có nhu cầu từ năm 2020. Hello
                Bác sĩ là dự án tâm huyết của những người sáng lập chúng tôi
                nhằm mang lại dịch vụ chăm sóc sức khỏe miễn phí đến tất cả các
                nơi trên Việt Nam. Chúng tôi tin rằng tất cả mọi người trên Việt
                Nam nên có khả năng kết nối với một bác sĩ có kinh nghiệm.
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default MakeAQuestion;
