/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useContext, useEffect } from 'react'
import { Button, Table, Avatar, message, Modal, Input, Space } from "antd";
import Highlighter from 'react-highlight-words';
import { PlusCircleOutlined, ExclamationCircleOutlined, DeleteOutlined, SearchOutlined } from "@ant-design/icons";

import { AuthContext } from '../../contexts/AuthContext';
import AddUser from './AdminModal/AddUser';
import UpdateUser from './AdminModal/UpdateUser';

import { AdminContext } from '../../contexts/AdminContext';

import "./style.css";


const UserManagement = () => {
  const [stateTable, setStateTable] = useState({
    dataTable: [],
    selectedRows: [],
    loading: true,
  });

  const [c, setC] = useState({
    searchText: '',
    searchedColumn: '',
  })

  const { confirm } = Modal;
  const [stateRecord, setStateRecord] = useState({})
  const { modalState, setModalState } = useContext(AdminContext);
  const { getAllUser, deleteUser } = useContext(AuthContext)

  const search = async () => {
    const data = await getAllUser()
    data && setStateTable({ ...stateTable, dataTable: data.user, loading: false })
  }

  useEffect(() => {
    search()
  }, [getAllUser, modalState])


  // Update user modal
  const showModalUpdateUser = () => {
    setModalState({ ...modalState, modalUpdateUser: true, showUpdateUser: true });
  }

  // Add user modal
  const showModalAddUser = () => {
    setModalState({ ...modalState, modalAddUser: true, showAddUser: true });
  };

  // Delete User
  const showConfirmDelete = () => {
    confirm({
      title: 'Xóa',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn chắc chắn muốn xóa những mục đã chọn?',
      cancelText: 'Hủy',
      okText: 'Đồng ý',
      async onOk() {
        const data = await deleteUser({ list: stateTable.selectedRows })
        await search()
        message.success(data.message)
      },
      onCancel() { },
    });
  }

  // Search 
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          // ref={node => {
          //   this.searchInput = node;
          // }}
          placeholder={`Tìm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Đặt lại
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setC({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button> */}
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    // onFilterDropdownVisibleChange: visible => {
    //   if (visible) {
    //     setTimeout(() => this.searchInput.select(), 100);
    //   }
    // },
    render: (text, record, index) =>
      c.searchedColumn === dataIndex ? (
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            showModalUpdateUser();
            setStateRecord(record)
          }}
        >
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[c.searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ''}
          />
        </a>
      ) : (
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            showModalUpdateUser();
            setStateRecord(record)
          }}
        >
          {text}
        </a>

      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setC({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  const handleReset = clearFilters => {
    clearFilters();
    setC({ searchText: '' });
  };


  const columns = [
    {
      title: 'STT',
      key: 'STT',
      render: (text, record, index) => index + 1,
      width: 60,
    },
    {
      title: 'Username',
      dataIndex: 'username',
      key: 'username',
      ...getColumnSearchProps('username'),
    },
    {
      title: 'Họ tên',
      dataIndex: 'fullName',
      key: 'fullName',
    },
    {
      title: 'Giới tính',
      dataIndex: 'sex',
      key: 'sex',
    },
    {
      title: 'Avatar',
      key: 'image',
      render: (text, record, index) =>
        <Avatar src={record.image} />
    },
    {
      title: 'Quyền',
      dataIndex: 'role',
      key: 'role',

    },
    {
      title: 'Đã tạo',
      dataIndex: 'createdAt',
      key: 'createdAt',
    },
  ];

  return (
    <div>
      <div className="lq_container min-h">
        <div className="page-c">
          <div className="page-c__left">Quản lý User</div>
          <div className="page-c__right">
            <Button type="link" onClick={showModalAddUser} ><PlusCircleOutlined style={{ color: "#1890ff" }} />Thêm</Button>
            {(stateTable.selectedRows.length != 0) ? <Button type="link" onClick={showConfirmDelete}><DeleteOutlined style={{ color: "rgb(255, 120, 117)" }} />Xóa</Button> : ""}
          </div>
        </div>

        <Table
          rowSelection={{
            selectedRowKeys: stateTable.selectedRows,
            onChange: (selectedRowKeys) => {
              setStateTable({
                ...stateTable,
                selectedRows: selectedRowKeys,
              });
            },
          }
          }
          showSorterTooltip={false}
          columns={columns}
          dataSource={stateTable.dataTable}
          bordered
          loading={stateTable.loading}
        />
      </div>
      {modalState.showAddUser && <AddUser />}
      {modalState.showUpdateUser && <UpdateUser dataProps={stateRecord} />}
    </div>
  )
}

export default UserManagement
