import React, { useState, useContext } from 'react'
import { Modal, Form, Input, Col, Radio, Row, Select, Button, message } from 'antd';
import FileBase from 'react-file-base64';

import { AuthContext } from '../../../contexts/AuthContext';
import { AdminContext } from '../../../contexts/AdminContext';

const AddUser = () => {
  const [AddUser, setAddUser] = useState({ loading: false })

  const [stateRegister, setStateRegister] = useState({
    role: 0,
    image: "",
  })

  const { registerByAdmin, findUser } = useContext(AuthContext);
  const { modalState, setModalState, openModal, setOpenModal } = useContext(AdminContext);

  const handleCancel = async () => {
    await setModalState({ ...modalState, showAddUser: false,modalAddUser: false });
  };

  const onFinish = async (values) => {
    await setAddUser({ loading: true });

    try {
      const data = await registerByAdmin({ ...values, ...stateRegister });
      if (data.success) {
        await setAddUser({ loading: false });
        await setModalState({ ...modalState, showAddUser: false, modalAddUser : false });
        await message.success(data.message);
      }
    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
    }
  };


  return (
    <Modal title="Thêm User" width={1000} visible={modalState.showAddUser} onCancel={handleCancel} footer={null} >
      <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}
      >
        <Row>
          <Col span={11} >
            <Form.Item
              name="username"
              label={<><span>Username</span> </>}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên tài khoản!',
                },
                () => ({
                  async validator(_, value) {
                    if (value !== undefined) {
                      if (/\s/.test(value)) {
                        return Promise.reject(new Error('Tên tài khoản phải không có ký tự khoảng trắng!'));
                      }

                      if (value.length < 6 || value.length >= 15) {
                        return Promise.reject(new Error('Tên tài khoản phải có 6-15 ký tự!'));
                      }

                      try {
                        const data = await findUser({ username: value })
                        if (!data.success) {
                          return Promise.reject(new Error('Tên tài khoản đã tồn tại!'))
                        }
                        else
                          return Promise.resolve();

                      } catch (error) {
                        console.log(error);
                      }
                    }
                  },
                })
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="password"
              label={<><span>Mật khẩu</span> </>}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu!',
                },
                () => ({
                  validator(_, value) {
                    if (!value || (value.length >= 8 && value.length <= 16)) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Mật khẩu phải đảm bảo độ dài 8-16 ký tự!'));
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="confirmPassword"
              label={<><span>Nhập lại mật khẩu</span> </>}
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng lại nhập mật khẩu!',
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(new Error('Hai mật khẩu bạn đã nhập không khớp'));
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="fullName"
              label={<><span>Họ tên</span> </>}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={2}> </Col>
          <Col span={11}>
            <Form.Item label="Thêm Avata">
              <FileBase type="file" multiple={false} value={stateRegister.image} onDone={({ base64 }) => setStateRegister({ ...stateRegister, image: base64 })} />
            </Form.Item>
            <Form.Item label="Quyền">
              <Select defaultValue="User" onChange={(e) => setStateRegister({ ...stateRegister, role: e })}>
                <Select.Option value={0} >User</Select.Option>
                <Select.Option value={1}>Bác sĩ</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item label="Giới tính" name="sex">
              <Radio.Group>
                <Radio value="Nam">Nam</Radio>
                <Radio value="Nữ">Nữ</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ borderTop: "1px solid #f0f0f0", paddingTop: "10px" }}>
          <Col span={18}> </Col>
          <Col span={6} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button style={{ marginRight: "10px" }} onClick={handleCancel}>Hủy bỏ</Button>
            <Button type="primary" htmlType="submit" loading={AddUser.loading}>
              Thêm
            </Button>
          </Col>
        </Row>
      </Form>
    </Modal>)
}

export default AddUser;