import React, { useState, useContext, useEffect } from 'react'
import { Modal, Form, Input, Col, Radio, Row, Select, Button, message } from 'antd';
import FileBase from 'react-file-base64';

import { PostContext } from '../../../contexts/PostContext';
import { AdminContext } from '../../../contexts/AdminContext';

const AddQuestion = () => {
  const [AddQuestion, setAddQuestion] = useState({ loading: false })
  const [listUser, setlistUser] = useState([])
  const [dataAsk, setdataAsk] = useState({
    userId: "",
    content: "",
    image: "",
    status: "approved"
  })

  const { modalState, setModalState, allUserAsk } = useContext(AdminContext);
  const { postQuestion } = useContext(PostContext)

  const handleCancel = async () => {
    await setModalState({ ...modalState, showAddQuestion: false , modalAddQuestion: false });
  };

  const onFinish = async (values) => {
    await setAddQuestion({ loading: true });
    try {
      const data = await postQuestion(dataAsk);
      if (data.success) {
        await setAddQuestion({ loading: false });
        await setModalState({ ...modalState, showAddQuestion: false, modalAddQuestion: false });
        await message.success(data.message);
      }
    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
      setAddQuestion({ loading: false });
    }
  };

  const getUser = async () => {
    const data = await allUserAsk();
    data ? setlistUser(data.user) : setlistUser(["Get user error"])

  }

  useEffect(() => {
    getUser()
  }, [])

  return (
    <Modal title="Thêm câu hỏi" width={800} visible={modalState.modalAddQuestion} onCancel={handleCancel} footer={null} >
      <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}
      >
        <Form.Item label={<> <span className="r">*</span> Chọn User đặt câu hỏi </>}>
          <Select
            placeholder="Chọn username"
            onChange={value => setdataAsk({ ...dataAsk, userId: value })}
          >
            {
              listUser.map((user) =>
                <Select.Option value={user._id}>{user.username}</Select.Option>
              )
            }
          </Select>
        </Form.Item>

        <Form.Item label={<> <span className="r">*</span> Nội dung câu hỏi </>}>
          <textarea name="" id="" className="input-comment" rows="4" value={dataAsk.content} onChange={(event) => setdataAsk({ ...dataAsk, content: event.target.value })} ></textarea>
        </Form.Item>

        <Form.Item label="Thêm ảnh">
          <FileBase type="file" multiple={false} value={dataAsk.image} onDone={({ base64 }) => setdataAsk({ ...dataAsk, image: base64 })} />
        </Form.Item>
        <Row style={{ borderTop: "1px solid #f0f0f0", paddingTop: "10px" }}>
          <Col span={18}> </Col>
          <Col span={6} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button style={{ marginRight: "10px" }} onClick={handleCancel}>Hủy bỏ</Button>
            <Button type="primary" htmlType="submit" loading={AddQuestion.loading}>
              Thêm
            </Button>
          </Col>
        </Row>

      </Form>
    </Modal>)
}

export default AddQuestion;