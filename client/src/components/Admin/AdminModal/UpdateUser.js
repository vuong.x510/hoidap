import React, { useState, useContext } from 'react'
import { Modal, Form, Input, Col, Radio, Row, Select, Button, Avatar, message } from 'antd';
import FileBase from 'react-file-base64';

import { AuthContext } from '../../../contexts/AuthContext';
import { AdminContext } from '../../../contexts/AdminContext';
import ImgUser from "../../../assets/avatar.png";


const UpdateUser = ({ dataProps }) => {
  const [load, setLoad] = useState({ loading: false })

  const [stateUpdate, setStateUpdate] = useState({
    ...dataProps
  })
  const [stateU, setStateU] = useState({
    role: stateUpdate.role,
    image: stateUpdate.image,
    _id: stateUpdate._id
  })
  const { updateUserByAdmin, findUser } = useContext(AuthContext);
  const { modalState, setModalState } = useContext(AdminContext);

  const handleCancel = () => {
    setModalState({ ...modalState, modalUpdateUser: false, showUpdateUser: false });
  };

  const onFinish = async (values) => {
    await setLoad({ loading: true });

    //delete
    Object.keys(values).forEach(key => values[key] === undefined && delete values[key])
    Object.keys(stateU).forEach(key => stateU[key] === undefined && delete stateU[key])

    try {
      const data = await updateUserByAdmin({ ...values, ...stateU });
      if (data.success) {
        await setLoad({ loading: false });
        await setModalState({ ...modalState, modalUpdateUser: false, showUpdateUser: false });
        await message.success(data.message);
      }
    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
    }
  };

  return (
    <Modal title="Cập nhật thông tin User" width={1000} visible={modalState.showUpdateUser} onCancel={handleCancel} footer={null} >
      <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}
        initialValues={{ sex: stateUpdate.sex, fullName: stateUpdate.fullName, username: stateUpdate.username }}
      >
        <Row>
          <Col span={11} >
            <Form.Item
              name="username"
              label={<><span>Username</span> </>}
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên tài khoản!',
                },
                () => ({
                  async validator(_, value) {
                    if (value !== undefined) {
                      if (/\s/.test(value)) {
                        return Promise.reject(new Error('Tên tài khoản phải không có ký tự khoảng trắng!'));
                      }

                      if (value.length < 6 || value.length >= 15) {
                        return Promise.reject(new Error('Tên tài khoản phải có 6-15 ký tự!'));
                      }
                      if (value != dataProps.username) {
                        try {
                          const data = await findUser({ username: value })
                          if (value != stateUpdate.username) {
                            if (!data.success) {
                              return Promise.reject(new Error('Tên tài khoản đã tồn tại!'))
                            }
                            else
                              return Promise.resolve();
                          }
                        } catch (error) {
                          console.log(error);
                        }
                      }
                    }
                  },
                })
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="password"
              label={<><span>Mật khẩu mới</span> </>}
              rules={[
                () => ({
                  validator(_, value) {
                    if (!value || (value.length >= 8 && value.length <= 16)) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Mật khẩu phải đảm bảo độ dài 8-16 ký tự!'));
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="confirmPassword"
              label={<><span>Nhập lại mật khẩu</span> </>}
              dependencies={['password']}
              rules={[
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(new Error('Hai mật khẩu bạn đã nhập không khớp'));
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="fullName"
              label={<><span>Họ tên</span> </>}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={2}> </Col>
          <Col span={11}>
            <Form.Item>
              <Avatar
                size={60}
                src={stateUpdate.image == undefined ? ImgUser : stateUpdate.image}
              />
            </Form.Item>
            <Form.Item label="Thay đổi Avatar">
              <FileBase type="file" multiple={false} value={stateU.image} onDone={({ base64 }) => setStateU({ ...stateUpdate, image: base64 })} />
            </Form.Item>
            <Form.Item label="Quyền">
              <Select defaultValue={stateU.role == 0 ? 'User' : 'Bác sĩ'} onChange={(e) => setStateU({ ...stateU, role: e })}>
                <Select.Option value={0} >User</Select.Option>
                <Select.Option value={1}>Bác sĩ</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item label="Giới tính" name="sex">
              <Radio.Group  >
                <Radio value="Nam">Nam</Radio>
                <Radio value="Nữ">Nữ</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row style={{ borderTop: "1px solid #f0f0f0", paddingTop: "10px" }}>
          <Col span={18}> </Col>
          <Col span={6} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button style={{ marginRight: "10px" }} onClick={handleCancel}>Hủy bỏ</Button>
            <Button type="primary" htmlType="submit">
              Cập nhật
            </Button>
          </Col>
        </Row>
      </Form>
    </Modal>)
}

export default UpdateUser;