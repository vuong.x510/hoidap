import React, { useState, useContext } from 'react'
import { Modal, Form, Input, Col, Select, Row, Image, Button, Avatar, message } from 'antd';
import FileBase from 'react-file-base64';

import { AdminContext } from '../../../contexts/AdminContext';
import { PostContext } from '../../../contexts/PostContext';



const UpdateQuestion = ({ dataProps }) => {
  const [load, setLoad] = useState({ loading: false })
  const [dataAsk, setdataAsk] = useState({
    userId: dataProps.user._id,
    questionId: dataProps._id,
    content: dataProps.content,
    image: dataProps.image,
    status: dataProps.status
  })
console.log(dataAsk);

  const { modalState, setModalState } = useContext(AdminContext);
  const {updateQuestion} = useContext(PostContext)

  const handleCancel = () => {
    setModalState({ ...modalState, modalUpdateQuestion: false, showUpdateQuestion: false });
  };

  const onFinish = async (values) => {
    await setLoad({ loading: true });

    try {
      const data = await updateQuestion(dataAsk);
      await setLoad({ loading: false });
      await setModalState({ ...modalState, modalUpdateQuestion: false, showUpdateQuestion: false });
      await message.success(data.message);

    } catch (error) {
      console.log(error)
      message.error('Hệ thống đang gập sự cố vui lòng thử lại sau!');
    }
  };

  return (
    <Modal title="Cập nhật câu hỏi" width={800} visible={modalState.showUpdateQuestion} onCancel={handleCancel} footer={null} >
      <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}

      >
        <Form.Item label="Username">
          <Input value={dataProps.username} disabled />
        </Form.Item>

        <Form.Item label="Nội dung câu hỏi">
          <textarea name="" id="" className="input-comment" rows="4" value={dataAsk.content} onChange={(event) => setdataAsk({ ...dataAsk, content: event.target.value })} ></textarea>
        </Form.Item>

        {dataAsk.image && <Form.Item>
          <Image
            width={200}
            src={dataAsk.image}
          />
        </Form.Item>}

        <Form.Item label="Thêm ảnh mới">
          <FileBase type="file" multiple={false} value={dataAsk.image} onDone={({ base64 }) => setdataAsk({ ...dataAsk, image: base64 })} />
        </Form.Item>

        <Form.Item label="Trạng thái">
          <Select
            defaultValue={dataAsk.status}
            onChange={value => setdataAsk({ ...dataAsk, status: value })}
          >
            <Select.Option value="approval">Approval</Select.Option>
            <Select.Option value="approved">Approved</Select.Option>
            <Select.Option value="hide">Hide</Select.Option>

          </Select>
        </Form.Item>

        <Row style={{ borderTop: "1px solid #f0f0f0", paddingTop: "10px" }}>
          <Col span={18}> </Col>
          <Col span={6} style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button style={{ marginRight: "10px" }} onClick={handleCancel}>Hủy bỏ</Button>
            <Button type="primary" htmlType="submit">
              Cập nhật
            </Button>
          </Col>
        </Row>
      </Form>
    </Modal>)
}

export default UpdateQuestion;