/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useContext, useEffect } from "react";
import { Button, Table, Avatar, message, Modal, Input, Space } from "antd";
import Highlighter from "react-highlight-words";
import {
  PlusCircleOutlined,
  ExclamationCircleOutlined,
  DeleteOutlined,
  SearchOutlined,
} from "@ant-design/icons";

import AddQuestion from "./AdminModal/AddQuestion";
import UpdateQuestion from "./AdminModal/UpdateQuestion";
import { PostContext } from "../../contexts/PostContext";

import { AdminContext } from "../../contexts/AdminContext";

import "./style.css";

const UserManagement = () => {
  const [stateTable, setStateTable] = useState({
    dataTable: [],
    selectedRows: [],
    loading: true,
  });

  const [c, setC] = useState({
    searchText: "",
    searchedColumn: "",
  });

  const { confirm } = Modal;
  const [stateRecord, setStateRecord] = useState({});
  const { modalState, setModalState } = useContext(AdminContext);

  const { getAdminAllPosts, deleteQuestion } = useContext(PostContext);

  const search = async () => {
    const data = await getAdminAllPosts();
    if (data !== undefined) {
      data.map((item) => {
        item.username = item.user.username;
        item.key = item._id;
        return item;
      });
      data && setStateTable({ ...stateTable, dataTable: data, loading: false });
    }
  };

  useEffect(() => {
    search();
  }, [getAdminAllPosts, modalState]);

  // Update modal
  const showModalUpdate = () => {
    setModalState({
      ...modalState,
      modalUpdateQuestion: true,
      showUpdateQuestion: true,
    });
  };

  // Add modal
  const showModalAdd = () => {
    setModalState({
      ...modalState,
      modalAddQuestion: true,
      showAddQuestion: true,
    });
  };

  // Delete User
  const showConfirmDelete = () => {
    confirm({
      title: "Xóa",
      icon: <ExclamationCircleOutlined />,
      content: "Bạn chắc chắn muốn xóa những mục đã chọn?",
      cancelText: "Hủy",
      okText: "Đồng ý",
      async onOk() {
        const data = await deleteQuestion({ list: stateTable.selectedRows });
        await search();
        message.success(data.message);
      },
      onCancel() {},
    });
  };

  // Search Username
  const getColumnSearchPropsUsername = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          // ref={node => {
          //   this.searchInput = node;
          // }}
          placeholder={`Tìm ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Đặt lại
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setC({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button> */}
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    // onFilterDropdownVisibleChange: visible => {
    //   if (visible) {
    //     setTimeout(() => this.searchInput.select(), 100);
    //   }
    // },
    render: (text) =>
      c.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[c.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const getColumnSearchPropsQuestion = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          // ref={node => {
          //   this.searchInput = node;
          // }}
          placeholder={`Tìm câu hỏi`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Đặt lại
          </Button>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setC({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button> */}
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    // onFilterDropdownVisibleChange: visible => {
    //   if (visible) {
    //     setTimeout(() => this.searchInput.select(), 100);
    //   }
    // },
    render: (text, record, index) =>
      c.searchedColumn === dataIndex ? (
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            showModalUpdate();
            setStateRecord(record);
          }}
          style={{ whiteSpace: "pre-wrap" }}
        >
          <Highlighter
            highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
            searchWords={[c.searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ""}
          />
        </a>
      ) : (
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            showModalUpdate();
            setStateRecord(record);
          }}
          style={{ whiteSpace: "pre-wrap" }}
        >
          {text}
        </a>
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setC({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setC({ searchText: "" });
  };

  const columns = [
    {
      title: "STT",
      key: "STT",
      render: (text, record, index) => index + 1,
      width: 60,
    },
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
      ...getColumnSearchPropsUsername("username"),
    },
    {
      title: "Câu hỏi",
      dataIndex: "content",
      key: "content",
      ...getColumnSearchPropsQuestion("content"),
    },
    {
      title: "Ảnh",
      key: "image",
      render: (text, record, index) => <Avatar src={record.image} />,
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Đã đặt câu hỏi",
      dataIndex: "createdAt",
      key: "createdAt",
    },
  ];

  return (
    <div>
      <div className="lq_container min-h">
        <div className="page-c">
          <div className="page-c__left">Quản lý câu hỏi</div>
          <div className="page-c__right">
            <Button type="link" onClick={showModalAdd}>
              <PlusCircleOutlined style={{ color: "#1890ff" }} />
              Thêm
            </Button>
            {stateTable.selectedRows.length != 0 ? (
              <Button type="link" onClick={showConfirmDelete}>
                <DeleteOutlined style={{ color: "rgb(255, 120, 117)" }} />
                Xóa
              </Button>
            ) : (
              ""
            )}
          </div>
        </div>

        <Table
          rowSelection={{
            selectedRowKeys: stateTable.selectedRows,
            onChange: (selectedRowKeys) => {
              setStateTable({
                ...stateTable,
                selectedRows: selectedRowKeys,
              });
            },
          }}
          showSorterTooltip={false}
          columns={columns}
          dataSource={stateTable.dataTable}
          bordered
          loading={stateTable.loading}
        />
      </div>
      {modalState.showAddQuestion && <AddQuestion />}
      {modalState.showUpdateQuestion && (
        <UpdateQuestion dataProps={stateRecord} />
      )}
    </div>
  );
};

export default UserManagement;
