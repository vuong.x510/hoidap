const jwt = require("jsonwebtoken");

const checkAdmin = async (req, res, next) => {
  try {
		if (req.userId != "60d59b61ab32880015d42f59") {
      next(new Error("Permission denied."));
      return;
		}
    next()
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: 'Internal server error' });
	}
}

module.exports = checkAdmin;
