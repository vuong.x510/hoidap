require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const authRouter = require("./routes/auth");
const authQuestions = require("./routes/questions");
const authComment = require("./routes/comment");
const notification = require("./routes/notification");

const connectDB = async () => {
  try {
    await mongoose.connect(
      `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.d3cs4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
      {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      }
    );
    console.log("Connected MongoDB ");
  } catch (error) {
    console.log(error.message);
    console.log("Connected MongoDB failed", process.env.DB_USERNAME);
    process.exit(1);
  }
};

connectDB();

const app = express();
app.use(express.json({ limit: "30mb" }));
app.use(express.urlencoded({ extended: true, limit: "30mb" }));
app.use(cors());

app.use("/api/auth", authRouter);
app.use("/api/questions", authQuestions);
app.use("/api/comment", authComment);
app.use("/api/notification", notification);

const PORT = process.env.PORT || 9000;

app.listen(PORT, () => console.log(`server running on port ${PORT}`));
