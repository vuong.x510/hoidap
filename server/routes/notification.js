const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/auth");

const Notification = require("../models/Notification");
const Questions = require("../models/Questions");
const User = require("../models/User");

// @route get api/notification
// @desc get notification
// @access Private
router.get("/", verifyToken, async (req, res) => {
  try {
    const notification = await Notification.find({ user: req.userId });
    res.json({ success: true, notification });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/notification
// @desc Create notification
// @access Private
// Checked
router.post("/myNotification", verifyToken, async (req, res) => {
  try {
    const newNotification = new Notification({
      ...req.body,
    });

    const data = await newNotification.save();
    res.json({ success: true, data, message: "Bạn có 1 thông báo mới" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

module.exports = router;
