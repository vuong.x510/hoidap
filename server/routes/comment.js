const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/auth");

const Comments = require("../models/Comments");
const Questions = require("../models/Questions");
const User = require("../models/User");

// @route GET api/comments
// @desc Get comment of question
// @access Public
router.get("/:id", async (req, res) => {
  try {
    const comment = await Comments.find({ question: req.params.id })
      .populate("questions", ["content", "image"])
      .populate("user", ["username", "fullName", "image", "role"]);
    res.json({ success: true, comment });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/comments
// @desc Create comments
// @access Private
// Checked
router.post("/:id", verifyToken, async (req, res) => {
  const { content, image } = req.body;

  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Content is required" });

  try {
    // Tach tham so
    let query = req.params.id.split("&");

    // Tim xem cau tra loi co ton tai hay khong
    const question = await Questions.findById(query[0]);
    const roleUser = await User.findById(query[1], "role");

    if (
      (question?.status === "approved" && question.user == req.userId) ||
      roleUser.role === 1
    ) {
      const newComment = new Comments({
        content,
        image,
        user: query[1],
        question: query[0],
      });

      await newComment.save();

      res.json({
        success: true,
        message: "Gửi thành công!",
        comment: newComment,
      });
    } else
      res.status(500).json({ success: false, message: "cau tra loi bi loi" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route PUT api/comments
// @desc Update comments
// @access Private
router.put("/:id", verifyToken, async (req, res) => {
  const { content, image } = req.body;

  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Content is required" });

  try {
    let updatedComment = {
      content,
      image,
    };

    const postUpdateCondition = { _id: req.params.id, user: req.userId };

    updatedComment = await Comments.findOneAndUpdate(
      postUpdateCondition,
      updatedComment,
      { new: true }
    );

    // User not authorized to update post or post not found
    if (!updatedComment)
      return res.status(401).json({
        success: false,
        message: "Questions not found or user not authorized",
      });

    res.json({
      success: true,
      message: "Excellent progress!",
      comment: updatedComment,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route DELETE api/comments
// @desc Delete comments
// @access Private
router.delete("/:id", verifyToken, async (req, res) => {
  try {
    const postDeleteCondition = { _id: req.params.id, user: req.userId };
    const deletedQuestion = await Comments.findOneAndDelete(
      postDeleteCondition
    );

    // User not authorized or comments not found
    if (!deletedQuestion)
      return res.status(401).json({
        success: false,
        message: "Comments not found or user not authorized",
      });

    res.json({ success: true, Comments: deletedQuestion });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

module.exports = router;
