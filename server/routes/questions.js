const express = require("express");
const router = express.Router();
const verifyToken = require("../middleware/auth");
const checkAdmin = require("../middleware/admin");
const mongoose = require("mongoose");
const Questions = require("../models/Questions");
const User = require("../models/User");

// @route GET api/questions
// @desc Get questions
// @access Private
router.get("/", verifyToken, async (req, res) => {
  try {
    const questions = await Questions.find({ user: req.userId }).populate(
      "user",
      ["username", "fullName"]
    );
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/questions/:id
// @desc Get one questions
// @access public
router.get("/one/:id", async (req, res) => {
  try {
    const questions = await Questions.findOne({ _id: req.params.id }).populate(
      "user",
      ["username", "fullName", "_id", "image"]
    );
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/question
// @desc Create questions
// @access Private
router.post("/", verifyToken, async (req, res) => {
  const user = await User.findById(req.userId);
  if (user.role === 1) {
    return res.status(500).json({
      success: false,
      message: "Tài khoản bác sĩ không thể đạt câu hỏi!",
    });
  }
  if (user.role === 2) {
    return res.status(500).json({
      success: false,
      message: "Tài khoản Admin không thể đạt câu hỏi!",
    });
  }

  const { content, image } = req.body;

  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Nội dung câu hỏi không được trống!" });

  try {
    const newQuestions = new Questions({
      content,
      image,
      user: req.userId,
    });

    const data = await newQuestions.save();
    res.json({
      data,
      success: true,
      message: "Đặt câu hỏi thành công, câu hỏi của bạn đang được duyệt!",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      success: false,
      message: "Hệ thống đang gập sự cố vui lòng thử lại sau!",
    });
  }
});

// @route PUT api/posts
// @desc Update question by admin
// @access Private
// Admin
router.put("/updateQuestion", verifyToken, checkAdmin, async (req, res) => {
  const { content, status, image, userId, questionId } = req.body;
  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Content is required" });

  try {
    let updatedQuestion = {
      content,
      image,
      status,
    };

    const postUpdateCondition = {
      _id: mongoose.Types.ObjectId(questionId),
      user: mongoose.Types.ObjectId(userId),
    };

    updatedQuestion = await Questions.findOneAndUpdate(
      postUpdateCondition,
      updatedQuestion,
      { new: true }
    );

    // User not authorized to update post or post not found
    if (!updatedQuestion)
      return res.status(401).json({
        success: false,
        message: "Câu hỏi không tồn tại hoặc không timg thấy User",
      });

    res.json({
      success: true,
      message: "Cập nhật thành công!",
      post: updatedQuestion,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route PUT api/posts
// @desc Update question
// @access Private
router.put("/:id", verifyToken, async (req, res) => {
  const { content, status, image } = req.body;

  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Content is required" });

  try {
    let updatedQuestion = {
      content,
      image,
      status: status || "approval",
    };

    const postUpdateCondition = { _id: req.params.id, user: req.userId };

    updatedQuestion = await Questions.findOneAndUpdate(
      postUpdateCondition,
      updatedQuestion,
      { new: true }
    );

    // User not authorized to update post or post not found
    if (!updatedQuestion)
      return res.status(401).json({
        success: false,
        message: "Questions not found or user not authorized",
      });

    res.json({
      success: true,
      message: "Excellent progress!",
      post: updatedQuestion,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route DELETE api/posts
// @desc Delete post
// @access Private
//admin
router.delete("/deleteQuestion", verifyToken, checkAdmin, async (req, res) => {
  try {
    const { list } = req.body;
    const status = await Questions.deleteMany({ _id: { $in: list } });

    if (!status)
      return res.status(401).json({
        success: false,
        message: "Khong tim thay cau hoi",
      });

    res.json({ success: true, message: "Xóa thành công!" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/questions/all
// @desc Get all questions
// @access public
router.get("/all", async (req, res) => {
  try {
    const questions = await Questions.find({ status: "approved" })
      .sort({ createdAt: -1 })
      .populate("user", ["username", "fullName", "image"]);
    // await questions
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/questions/all
// @desc Get all questions
// @access private
// Doctor
router.get("/allByDoctor", verifyToken, async (req, res) => {
  try {
    const user = await User.findById(req.userId).select(
      "-password, -username, -image, -createdAt, -sex"
    );
    if (!user) {
      return res
        .status(400)
        .json({ success: false, message: "User not found" });
    }

    if (user.role != 1) {
      return res.status(400).json({ success: false, message: "not found" });
    }

    const questions = await Questions.find({ status: ["approved", "approval"] })
      .sort({ status: 1 })
      .populate("user", ["username", "fullName", "image"]);
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route PUT api/posts
// @desc Update question
// @access Private
// Doctor
router.put("/doctorUpdate/:id", verifyToken, async (req, res) => {
  const { status } = req.body;

  // Simple validation
  if (!status)
    return res
      .status(400)
      .json({ success: false, message: "Status is required" });

  try {
    const user = await User.findById(req.userId).select(
      "-password, -username, -image, -createdAt, -sex"
    );
    if (!user) {
      return res
        .status(400)
        .json({ success: false, message: "User not found" });
    }

    if (user.role != 1) {
      return res.status(400).json({ success: false, message: "not found" });
    }

    let updatedQuestion = {
      status: status,
    };

    const postUpdateCondition = { _id: req.params.id };

    updatedQuestion = await Questions.findOneAndUpdate(
      postUpdateCondition,
      updatedQuestion,
      { new: true }
    );

    // User not authorized to update post or post not found
    if (!updatedQuestion)
      return res.status(401).json({
        success: false,
        message: "Không tìm thấy câu hỏi cần update!",
      });

    res.json({
      success: true,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/questions/myAll
// @desc Get all questions
// @access private
router.get("/myAll", verifyToken, async (req, res) => {
  try {
    const questions = await Questions.find({ user: req.userId })
      .sort({ createdAt: -1 })
      .populate("user", ["username", "fullName", "image"]);
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route GET api/questions/myAll
// @desc Get all questions
// @access private
// Admin
router.get("/adminAll", verifyToken, checkAdmin, async (req, res) => {
  try {
    const questions = await Questions.find().populate("user", [
      "username",
      "image",
    ]);
    res.json({ success: true, questions });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Internal server error" });
  }
});

// @route POST api/question
// @desc Create questions by admin
// @access Private
// Admin
router.post("/postQuestion", verifyToken, checkAdmin, async (req, res) => {
  const { content, image, userId, status } = req.body;
  // Simple validation
  if (!content)
    return res
      .status(400)
      .json({ success: false, message: "Nội dung câu hỏi không được trống!" });

  if (!userId)
    return res
      .status(400)
      .json({ success: false, message: "Người đặt câu hỏi không được trống!" });

  try {
    const newQuestions = new Questions({
      content,
      image,
      status,
      user: userId,
    });

    await newQuestions.save();

    res.json({
      success: true,
      message: "Đặt câu hỏi thành công, câu hỏi của bạn đang được duyệt!",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      success: false,
      message: "Hệ thống đang gập sự cố vui lòng thử lại sau!",
    });
  }
});

module.exports = router;
