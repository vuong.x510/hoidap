const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const verifyToken = require('../middleware/auth');
const checkAdmin = require('../middleware/admin');


const User = require("../models/User");

// @router GET api/auth
// @desc Check if user is logged in
// @access Public
router.get('/', verifyToken, async (req, res) => {
	try {
		const user = await User.findById(req.userId).select('-password')
		if (!user) {
			return res.status(400).json({ success: false, message: 'User not found' });
		}

		res.json({ success: true, user });
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: 'Internal server error' });
	}
});

// @router POST api/auth/find
// @desc Find user
// @access Public
router.post('/find', async (req, res) => {
	const { username } = req.body;
	try {
		// Check for existing user
		const user = await User.findOne({ username });

		if (user) {
			return res.status(400).json({ success: false, message: "Username already taken" });
		}

		res.json({ success: true, message: "User does exist" });
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
});

// @router POST api/auth/register
// @desc Register user
// @access Public
router.post('/register', async (req, res) => {
	const { username, password } = req.body;

	//Simple validation
	if (!username || !password)
		return res
			.status(400)
			.json({ success: false, message: "Missing usermane and/or password" })

	try {
		// Check for existing user
		const user = await User.findOne({ username });

		if (user) {
			return res.status(400).json({ success: false, message: "Username already taken" });
		}

		// All good
		const hashedPassword = await argon2.hash(password);
		const newUser = new User({ username, password: hashedPassword, image: '' });
		await newUser.save();

		// Return token
		const accessToken = jwt.sign(
			{ userId: newUser._id },
			process.env.ACCESS_TOKEN_SECRET
		);

		res.json({ success: true, message: "User created successfully", accessToken });
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
});

// @router POST api/auth/login
// @desc Login user
// @access Public
router.post('/login', async (req, res) => {
	const { username, password } = req.body;

	//Simple validation
	if (!username || !password) {
		return res.status(400).json({ success: false, message: "Missing usermane and/or password" });
	}

	try {
		// Check for existing user
		const user = await User.findOne({ username });
		if (!user) {
			return res.status(400).json({ success: false, message: "Thông tin tài khoản sai hoặc mật khẩu không chính xác!" });
		}
		// Username found
		const passwordValid = await argon2.verify(user.password, password)
		if (!passwordValid)
			return res
				.status(400)
				.json({ success: false, message: 'Thông tin tài khoản sai hoặc mật khẩu không chính xác!' })


		// All good
		// Return token
		const accessToken = jwt.sign({
			userId: user._id
		}, process.env.ACCESS_TOKEN_SECRET);

		res.json({
			success: true,
			message: 'User logged in successfully',
			accessToken
		});
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: "Internal server error" });
	}

});

// @route GET api/auth/all
// @desc Get all users
// @access Private
// @admin
router.get('/all', verifyToken, checkAdmin, async (req, res) => {
	try {
		// Tao them key
		const user = await User.aggregate([
			{ "$project": { "key": "$_id", "username": 1, "fullName": 1, "sex": 1, "image": 1, "role": 1, "createdAt": 1 } }
		])
		res.json({ success: true, user })
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: 'Internal server error' });
	}
})

// @route GET api/auth/all
// @desc Get all users have role = 0
// @access Private
// @admin
router.get('/allUserAsk', verifyToken, checkAdmin, async (req, res) => {
	try {
		const user = await User.find({role: 0}).select('-password -image -role -createdAt -fullName -sex -__v')
		res.json({ success: true, user })
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: 'Internal server error' });
	}
})

// @route POST api/auth/all
// @desc POST all users
// @access Private
// @admin
router.post('/registerByAdmin', verifyToken, checkAdmin, async (req, res) => {
	const { username, password, role, sex, fullName, image } = req.body;
	try {
		// Check for existing user
		const user = await User.findOne({ username });

		if (user) {
			return res.status(400).json({ success: false, message: "Username already taken" });
		}

		// All good
		const hashedPassword = await argon2.hash(password);
		const newUser = new User({ username, password: hashedPassword, role, sex, fullName, image });
		await newUser.save();

		res.json({ success: true, message: "Thêm user thành công!", newUser });
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
});

// @route PUT api/auth/all
// @desc PUT all users
// @access Private
// @admin
router.put("/updateUserByAdmin", verifyToken, checkAdmin, async (req, res) => {
	const { _id, username, role, ...userReq } = req.body;
	try {
		// Simple validation
		if (!username)
			return res
				.status(400)
				.json({ success: false, message: 'Content is required' });
		// password change?
		if (userReq.password == undefined || userReq.password == '') {
			const newUser = await User.findByIdAndUpdate(_id, { username, role, ...userReq });
			res.json({ success: true, message: "Cập nhật thành công!", newUser });
		}
		else {
			// password godd
			if (userReq.password.length >= 8 && userReq.password.length <= 16) {
				const hashedPassword = await argon2.hash(userReq.password);
				const newUser = await User.findByIdAndUpdate(_id, { ...userReq, username, role, password: hashedPassword });
				res.json({ success: true, message: "Cập nhật thành công!", newUser });
			}
			else {
				res.status(401).json({ success: false, message: "Mat khau " });
			}
		}


	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
});

// @route DELETE api/user
// @desc Delete user
// @access Private
// @admin
router.delete('/deleteUser', async (req, res) => {
	try {
		const { list } = req.body;
		const status = await User.deleteMany({ _id: { $in: list } });

		if (!status)
			return res.status(401).json({
				success: false,
				message: 'Khong tim thay user'
			})

		res.json({ success: true, message: "Xóa thành công!" });
	} catch (error) {
		console.log(error);
		res.status(500).json({ success: false, message: 'Internal server error' });
	}
});

module.exports = router;

