const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuestionsSchema = new Schema({
  content: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: ["approval", "approved", "hide"],
    default: "approval"
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
  },
  // comment: {
  //   type: Schema.Types.question,
  //   ref: "comments",
  //   required: true,
  // },
  image: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("questions", QuestionsSchema);