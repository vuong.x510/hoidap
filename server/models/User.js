const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  specialist: {
    type: String,
  },
  fullName: {
    type: String,
  },
  sex: {
    type: String,
    enum: ["Nam", "Nữ"],
  },
  image: {
    type: String,
  },
  role: {
    type: Number,
    enum: [0, 1, 2],
    default: 0
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
})

module.exports = mongoose.model("users", UserSchema);